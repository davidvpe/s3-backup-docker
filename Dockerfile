FROM debian:bookworm-slim

ENV S3_FOLDER_DEST=""
ENV S3_BUCKET=""
ENV MINIO_ENDPOINT=""
ENV MINIO_ACCESS_KEY=""
ENV MINIO_SECRET_KEY=""

ARG DOCKER_BUILDTIME
ENV DOCKER_BUILDTIME=${DOCKER_BUILDTIME}

RUN echo "DOCKER_BUILDTIME=${DOCKER_BUILDTIME}" > /etc/docker_buildtime


RUN apt-get update \
  && apt-get install -y --no-install-recommends curl inotify-tools ca-certificates \
  && curl -O https://dl.min.io/client/mc/release/linux-amd64/mc \
  && chmod +x mc \
  && mv mc /usr/local/bin/ \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]