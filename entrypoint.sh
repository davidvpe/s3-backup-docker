#!/bin/sh

echo "Starting entrypoint.sh"

DOCKER_BUILDTIME=$(cat /etc/docker_buildtime)

echo "Build time is: $DOCKER_BUILDTIME"

log() {
  echo "$(date -u +"%Y-%m-%dT%H:%M:%SZ") - $1"
}

for var in S3_BUCKET MINIO_ENDPOINT MINIO_ACCESS_KEY MINIO_SECRET_KEY; do
  if [ -z "$(eval echo \$$var)" ]; then
    log "$var is not set. Exiting."
    exit 1
  fi
done

# Configure MinIO client
mc alias set peru $MINIO_ENDPOINT $MINIO_ACCESS_KEY $MINIO_SECRET_KEY

log "Starting to monitor /from for changes..."

inotifywait -m -r -e modify,create,delete /from | while read path action file; do
  log "Detected $action on $file in $path"
  log "Syncing /from to peru/$S3_BUCKET"
  mc mirror /from peru/$S3_BUCKET --remove --summary --overwrite
done
