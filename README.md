# Build image

docker buildx build --platform linux/amd64 --push -t davidvpe/s3-backup .

# Run image
```bash
docker run -d \
    -e MINIO_ENDPOINT=https://minio.example.com \
    -e MINIO_ACCESS_KEY=YOUR_MINIO_ACCESS_KEY \
    -e MINIO_SECRET_KEY=YOUR_MINIO_SECRET_KEY \
    -e S3_FOLDER_DEST=backup-folder \
    -e S3_BUCKET=my-bucket \
    -v /host/folder_to_backup:/from \
  davidvpe/s3-backup